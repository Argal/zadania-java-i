/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zadania3;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Argal
 */
public class FunkcjeMain extends Funkcje{
    
    public static void main(String[] args){
        
       FunkcjeMain menu = new FunkcjeMain();
       Scanner scan = new Scanner(System.in);
       int wybor;
       
        System.out.println("||||||||||||||||||||||||||");
        System.out.println("| Wybierz funkcję z menu:|");
        System.out.println("|        Menu:           |");
        System.out.println("| 1, 2, 3, 4, 5, 6, 7, 8 |");
        System.out.println("||||||||||||||||||||||||||");
        

        try {
            wybor = scan.nextInt();
            
            switch(wybor){
                case 1: menu.fJeden();
                    break;
                case 2: menu.fDwa();
                    break;
                case 3: menu.fTrzy();
                    break;
                case 4: menu.fCztery();
                    break;
                case 5: menu.fPiec();
                    break;
                case 6: menu.fSzesc();
                    break;
                case 7: menu.fSiedem();
                    break; 
                case 8: menu.fOsiem();
                    break;
                default: System.out.println("Brak wyników w MENU.");
                    break;
            }
        }catch(InputMismatchException e){
            System.out.println("Brak wyników w MENU.");
        }
  
    }
    
}
